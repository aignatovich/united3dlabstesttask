using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;

public class CreateRoadSplineFromFile : ScriptableWizard
{
    public TextAsset file;
    public RoadSpline spline;
	public bool loop;

    [MenuItem("Custom/CreateRoadSplineFromFile")]
    static void DoReplace()
    {
        DisplayWizard<CreateRoadSplineFromFile>("CreateRoadSplineFromFile", "Create");
    }

    public void OnWizardCreate()
    {
        StringReader reader = new StringReader(file.text);
        reader.ReadLine();
        int pointCount = int.Parse(reader.ReadLine());
        List<Vector3> points = new List<Vector3>();

        for(int i = 0; i < pointCount; ++i)
        {
            string curline = reader.ReadLine();
            string[] floats = curline.Split(' ');
            float x = float.Parse(floats[0]);
            float y = float.Parse(floats[1]);
            float z = float.Parse(floats[2]);
            points.Add(new Vector3(-x, z, -y));
        }
		
		if(loop)
        	points.Add(points[0]);

        if(spline)
        {
            spline.Points = points.ToArray();
        }
    }
}