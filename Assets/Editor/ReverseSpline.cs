using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;

public class ReverseSpline : ScriptableWizard
{
    public RoadSpline spline;

    [MenuItem("Custom/ReverseRoadSpline")]
    static void DoReverse()
    {
        DisplayWizard<ReverseSpline>("ReverseRoadSpline", "Create");
    }

    public void OnWizardCreate()
    {
        Vector3[] points = spline.Points;
		List<Vector3> pl = new List<Vector3>(points);
		pl.Reverse();
        spline.Points = pl.ToArray();
    }
}