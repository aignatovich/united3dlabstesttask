﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;

public class ScaleRoadSpline : ScriptableWizard
{
    public float scale = 1f;
    public RoadSpline spline;

    [MenuItem("Custom/ScaleRoadSpline")]
    static void DoReplace()
    {
        DisplayWizard<ScaleRoadSpline>("ScaleRoadSpline", "Create");
    }

    public void OnWizardCreate()
    {
        Vector3[] points = spline.Points;

        for(int i = 0; i < points.Length; ++i)
        {
            points[i] = points[i] * scale;
        }

        spline.Points = points;
    }
}