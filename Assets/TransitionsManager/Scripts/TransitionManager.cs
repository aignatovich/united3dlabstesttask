// *********************************************************************************************************
// * Code belonging to DFT Games and released under use licence as per Unity Asset Store Licence Agreement *
// *********************************************************************************************************

using System.Collections.Generic;
using UnityEngine;

public class TransitionManager : MonoBehaviour
{
    // NetworkConstants and statics
    private const string STR_FadingTexture = "Fading texture";
    private static Vector3 fadingPosition = new Vector3(0.5f, 0.5f, 10f);
    private static Vector3 ratioIndependentScale = new Vector3(1f, 1f, 1f);

    // Public members
    public float SplashScreenFadingTime = 1.5f;
    public string NextScene = string.Empty;
    public Texture PitchBlackTexture = null;

    // Private members
    private static TransitionManager instance = null;
    private bool fadeInScene = false;
    private bool fadeOutScene = false;
    private GameObject fadingTexture = null;
    private Color color = Color.black;

    /// <summary>
    /// Instance of the Singleton Transition Manager
    /// used to talk to it from other game scripts
    /// </summary>
    public static TransitionManager Instance
    {
        get
        {
            return instance;
        }
    }

    void Awake()
    {
        // This test is here to prevent problems if 
        // by mistake one puts the script in more than one scene
        if (instance != null && instance != this)
        {
            Destroy(this);
            return;
        }
        instance = this;
        DontDestroyOnLoad(this);
    }

    void Start()
    {
        if (Camera.main != null)
            Camera.main.backgroundColor = Color.black;
		
        fadingTexture = new GameObject(STR_FadingTexture);
        fadingTexture.AddComponent<GUITexture>();
        DontDestroyOnLoad(fadingTexture);
		DoSplashScreens();
    }

    private void DoSplashScreens()
    {
        // prepare the fading GUITexture
        fadingTexture.guiTexture.texture = PitchBlackTexture;
        fadingTexture.guiTexture.color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
        fadingTexture.guiTexture.pixelInset = new Rect(-PitchBlackTexture.width / 2, -PitchBlackTexture.height / 2, PitchBlackTexture.width, PitchBlackTexture.height);
        fadingTexture.transform.position = fadingPosition;
        fadingTexture.transform.localScale = ratioIndependentScale;
    }

    void Update()
    {
        if (fadeInScene)
        {
            color = fadingTexture.guiTexture.color;
            color.a -= Time.deltaTime / (SplashScreenFadingTime * 2f);
            if (color.a <= 0f)
            {
                color.a = 0f;
                fadeInScene = false;
            }
            fadingTexture.guiTexture.color = color;
        }
        else if (fadeOutScene)
        {
            color = fadingTexture.guiTexture.color;
            color.a += Time.deltaTime / (SplashScreenFadingTime * 2f);
            if (color.a >= 0.5f)
            {
                color.a = 0.5f;
                fadeOutScene = false;
                LoadScene(NextScene, false);
            }
            fadingTexture.guiTexture.color = color;
        }
    }

    public void LoadScene(string sceneToLoad, bool fadeOutFirst)
    {	
        NextScene = sceneToLoad;
        if (fadeOutFirst)
        {
            fadeOutScene = true;
            return;
        }
        
        {
            if (NextScene != null && NextScene != string.Empty)
                Application.LoadLevel(NextScene);
            fadeInScene = true;
        }
    }
}
