﻿using UnityEngine;
using System.Collections;

public class CameraFollowShip : MonoBehaviour
{
    [SerializeField]
    private Transform m_target;
    [SerializeField]
    private float m_distance = 5f;
    [SerializeField]
    private float m_minDistance = 2f;
    [SerializeField]
    private float m_maxDistance = 12f;
    [SerializeField]
    private float m_alignSpeed = 4f;
    [SerializeField]
    private float m_height = 1.2f;
    [SerializeField]
    private float m_targetOffsetY = 0.2f;

    private Transform m_transform;
    private Vector3 m_oldTargetPosition;
    private Vector3 m_oldTargetVel;
    private Vector3 m_oldTargetAccel;
    private float m_curDist;
    private Vector3 m_forward;
    private Vector3 m_up;

    public void SetTarget(Transform target)
    {
        m_target = target;
        m_oldTargetPosition = m_target.position;
        m_forward = m_target.forward;
        m_up = m_target.up;
        m_curDist = m_distance;
    }

    void Awake()
    {
        m_transform = transform;
    }

    void Start()
    {
        if(m_target)
            SetTarget(m_target);
    }

    void LateUpdate()
    {
        if(!m_target || Time.smoothDeltaTime == 0)
            return;

        float dt = Time.smoothDeltaTime;

        Vector3 targetVel = (m_target.position - m_oldTargetPosition) / dt;
        Vector3 targetAccel = (targetVel - m_oldTargetVel) / dt;
        targetAccel = Vector3.Lerp(m_oldTargetAccel, targetAccel, dt);

        m_oldTargetPosition = m_target.position;
        m_oldTargetVel = targetVel;
        m_oldTargetAccel = targetAccel;

        Vector3 forward = m_target.forward;
        float longTargetAccel = Vector3.Dot(forward, targetAccel);
        longTargetAccel = Mathf.Clamp(longTargetAccel, -10f, 10f);

        float camDist = m_distance + longTargetAccel * 0.01f;
        camDist = Mathf.Clamp(camDist, m_minDistance, m_maxDistance);
        m_curDist += (camDist - m_curDist) * dt;
        m_curDist = Mathf.Clamp(m_curDist, m_minDistance, m_maxDistance);

        Vector3 up = m_target.up;

        Vector3 dir = targetVel - up * Vector3.Dot(targetVel, up);
        dir = Vector3.ClampMagnitude(dir, 10f);

        Vector3 fwmbe = forward * 0.1f + dir * 0.01f * dir.magnitude;
        fwmbe -= up * Vector3.Dot(fwmbe, up);
        fwmbe.Normalize();

        m_forward = Vector3.Slerp(m_forward, fwmbe, m_alignSpeed * dt);
        m_forward.Normalize();

        m_up = Vector3.Slerp(m_up, up, m_alignSpeed * dt);
        m_up.Normalize();

        Vector3 target = m_target.position + up * m_targetOffsetY;

        m_transform.position = target - m_forward * m_curDist + m_up * m_height;
        m_transform.LookAt(target, m_up);
    }
}