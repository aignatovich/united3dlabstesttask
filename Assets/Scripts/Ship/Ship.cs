﻿using System;
using UnityEngine;

public class Ship : MonoBehaviour
{
    [SerializeField]
    private GameObject m_shipRoot;
    [SerializeField]
    private float m_speed;
    [SerializeField]
    private float m_height;
    [SerializeField]
    private ShipControl m_control;
    [SerializeField]
    private CollisionDetecter m_collisionDetecter;
    [SerializeField]
    private float m_radius;

    private SyncedTime m_syncedTime;
    private Track m_track;
    private SpaceSceneModel m_sceneModel;
    private string m_modelId;

    private Vector3 m_trackPosition;
    private float m_bonusSpeed = 1;
    private bool m_isStartRun;
    private float m_distanceCorrection;

    private ShipsManager m_shipsManager;
    private bool m_isFinished;

    private void Awake()
    {
        m_shipsManager = FindObjectOfType<ShipsManager>();
        m_shipsManager.InitShip(this);
    }

    public void Init(string modelId, SpaceSceneModel sceneModel, Track track, SyncedTime syncedTime)
    {
        m_syncedTime = syncedTime;
        m_modelId = modelId;
        m_track = track;
        m_trackPosition = m_track.GetTrackPosition(transform.position);
        m_sceneModel = sceneModel;
        m_collisionDetecter.Init(m_radius);
    }

    public void StartMove()
    {
        m_isStartRun = true;
    }

    private void Update()
    {
        if(!m_isStartRun)
        {
            return;
        }

        Vector3 offset = Vector3.zero;
        offset.x = m_control.GetHorizontalAxies(transform.position, m_shipRoot.transform.right);
        offset.z = m_speed * m_bonusSpeed * (float) m_syncedTime.DeltaTime + m_distanceCorrection;
        m_distanceCorrection = 0;

        if(m_isFinished)
        {
            return;
        }

        if(m_trackPosition.z + offset.z >= m_track.endDistance + m_track.offsetFromEnd)
        {
            Finish();
            return;
        }

        offset = m_collisionDetecter.ClampDistanceOnTrack(transform.position, offset, m_shipRoot.transform.forward, m_shipRoot.transform.right);

        m_trackPosition += offset;

        Vector3 cnt;
        Vector3 right;
        Vector3 up;
        Vector3 front;
        m_track.GetBasisAtDistance(m_trackPosition.z, offset.z, out cnt, out right, out up, out front);

        bool leftClamp;
        bool rightClamp;
        bool heavy;
        m_trackPosition = m_track.BorderClampPosition(m_trackPosition, out leftClamp, out rightClamp, out heavy);

        transform.position = cnt + right * m_trackPosition.x + up * m_height;
        m_shipRoot.transform.rotation = Quaternion.LookRotation(front, up);
    }

    private void Finish()
    {
        m_isFinished = true;
        m_sceneModel.PlayerModels[m_modelId].FinishTime = m_syncedTime.ElapsedTime;
        m_shipsManager.ShipFinish(m_modelId);
    }

    [RPC]
    public void AddBonusSpeed(float bonusSpeed, int score, string collectTime)
    {
        m_distanceCorrection = m_speed * bonusSpeed * (float) (Network.time - Double.Parse(collectTime));
        m_bonusSpeed += bonusSpeed;
        m_sceneModel.PlayerModels[m_modelId].Score.Value += score;
    }

    public GameObject GetShipRoot()
    {
        return m_shipRoot;
    }
}