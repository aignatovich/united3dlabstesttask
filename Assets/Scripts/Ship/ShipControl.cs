﻿using UnityEngine;

[RequireComponent(typeof ( NetworkView ))]
public class ShipControl : MonoBehaviour
{
    [SerializeField]
    private NetworkView m_networkView;
    [SerializeField]
    private CollisionDetecter m_collisionDetecter;

    private float m_cachedHorizontalAxies;

    public float GetHorizontalAxies(Vector3 position, Vector3 direction)
    {
        if(m_networkView.isMine)
        {
            var newHorizontalAxies = Input.GetAxis(InputAxiesNames.GorizontalInputAxisName);
            if(newHorizontalAxies == 0)
            {
                return 0;
            }

            newHorizontalAxies = m_collisionDetecter.ClampDistanceToCollision(position, direction, newHorizontalAxies);
            m_networkView.RPC("AddHorizontalAxies", RPCMode.Others, newHorizontalAxies);
            m_cachedHorizontalAxies += newHorizontalAxies;
        }

        var result = m_cachedHorizontalAxies;
        m_cachedHorizontalAxies = 0;
        return result;
    }

    [RPC]
    private void AddHorizontalAxies(float horizontalAxies)
    {
        m_cachedHorizontalAxies += horizontalAxies;
    }
}
