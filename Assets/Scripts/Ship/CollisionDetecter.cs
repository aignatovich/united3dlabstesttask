﻿using UnityEngine;

public class CollisionDetecter : MonoBehaviour
{
    [SerializeField]
    private NetworkView m_networkView;

    private Vector3 m_correction;
    private float m_radius;

    public void Init(float radius)
    {
        m_radius = radius;
    }

    public Vector3 ClampDistanceOnTrack(Vector3 cnt, Vector3 offset, Vector3 forward, Vector3 right)
    {
        if(Network.isServer)
        {
            m_correction = CalculateOffsetCorrection(cnt, offset, forward, right);
            if(m_correction != Vector3.zero)
            {
                m_networkView.RPC("RecieveCorrection", RPCMode.Others, m_correction);
            }
        }

        offset -= m_correction;
        m_correction = Vector3.zero;

        return offset;
    }

    private Vector3 CalculateOffsetCorrection(Vector3 cnt, Vector3 offset, Vector3 forward, Vector3 right)
    {
        Vector3 correction = new Vector3();
        correction.z = offset.z - ClampDistanceToCollision(cnt, forward, offset.z);
        return correction;
    }

    public float ClampDistanceToCollision(Vector3 position, Vector3 direction, float distance)
    {
        if(distance == 0)
        {
            return distance;
        }

        float distanceSign = distance / Mathf.Abs(distance);
        distance = Mathf.Abs(distance);

        Debug.DrawRay(position, direction * distanceSign, Color.red, 20);

        RaycastHit hit;
        Ray ray = new Ray(position, direction * distanceSign);
        if (Physics.Raycast(ray, out hit, 2 * m_radius, 1 << LayerMask.NameToLayer(Layers.ShipLayerName)))
        {
            if(hit.distance < (distance + m_radius))
            {
                return distanceSign * (hit.distance - m_radius);
            }
        }

        return distanceSign * distance;
    }

    [RPC]
    private void RecieveCorrection(Vector3 correction)
    {
        m_correction += correction;
    }
}