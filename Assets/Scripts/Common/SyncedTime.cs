﻿using UnityEngine;

public class SyncedTime : MonoBehaviour
{
    public double DeltaTime { get; private set; }

    public double ElapsedTime
    {
        get
        {
            return Network.time - m_startTime;
        }
    }

    private double m_previosTime;
    private double m_startTime = double.MaxValue;

    public void SetStartTime(double startTime)
    {
        m_startTime = startTime;
        m_previosTime = m_startTime;
    }

    public void Elapse()
    {
        if(Network.time < m_startTime)
        {
            return;
        }

        DeltaTime = Network.time - m_previosTime;
        m_previosTime = Network.time;
    }
}