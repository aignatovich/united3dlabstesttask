using UnityEngine;

public class SpawnBonus : MonoBehaviour
{
	[SerializeField]
	private GameObject m_LGBonus;
	[SerializeField]
	private GameObject m_bombBonus;
	[SerializeField]
	private float m_LGBonusPriority = 0.7f;
	[SerializeField]
	private Track m_track;
	[SerializeField]
	private float m_sideOffset = 2.5f;
	[SerializeField]
	private float m_distanceBetweenBonusInRow = 1f;
	[SerializeField]
	private int m_maxBonusCountInRowSegment = 5;
	[SerializeField]
	private float m_minDistanceBetweenSegments = 30f;
	[SerializeField]
	private float m_maxDistanceBetweenSegments = 50f;
	[SerializeField]
	private float m_heightBonusOnTrack = .5f;
	
	enum BonusType
	{
		LG,
		Bomb
	}

    private void OnNetworkLoadedLevel()
    {
        if(Network.isServer)
        {
            float length = m_track.endDistance - 30f;
            float i = m_maxDistanceBetweenSegments;

            while (i < length)
            {
                int numRows = Random.Range(1, 4);
                int startRowIdx = Random.Range(0, 3);

                for (int r = 0; r < numRows; ++r)
                {
                    int bonusCount = Random.Range(1, m_maxBonusCountInRowSegment + 1);
                    BonusType bType = (Random.value > (1f - m_LGBonusPriority)) ? BonusType.LG : BonusType.Bomb;
                    float i2 = Random.Range(0, m_maxBonusCountInRowSegment - bonusCount) * m_distanceBetweenBonusInRow;

                    for (int b = 0; b < bonusCount; ++b)
                    {
                        if((i + i2) < length)
                            PlaceBonus(bType, i + i2, RepeatRow(startRowIdx + r));

                        i2 += m_distanceBetweenBonusInRow;
                    }
                }

                i += m_distanceBetweenBonusInRow * m_maxBonusCountInRowSegment;
                i += Random.Range(m_minDistanceBetweenSegments, m_maxDistanceBetweenSegments);
            }
        }
    }

    private int RepeatRow(int rowIdx)
	{
		return rowIdx % 3;
	}
	
	private void PlaceBonus(BonusType bType, float distance, int row)
	{
		Vector3 cnt, right, up, front;
		m_track.GetBasisAtDistance(distance, 0.1f, out cnt, out right, out up, out front);
		
		Vector3 position = cnt + right * (-1 + row) * m_sideOffset + up * m_heightBonusOnTrack;
		Quaternion rotation = Quaternion.LookRotation(front, up);
		
		
		InstantiateBonus((int)bType, position, rotation);
	}
	
	private void InstantiateBonus(int type, Vector3 position, Quaternion rotation)
	{
		BonusType bType = (BonusType)type;
		GameObject prefab = (bType == BonusType.LG) ? m_LGBonus : m_bombBonus;
		Network.Instantiate(prefab, position, rotation, 0);
	}
}