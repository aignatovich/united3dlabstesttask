﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof ( NetworkView ))]
public class MainMenuScreen : MonoBehaviour
{
    [SerializeField]
    private Text m_networkStatus;
    [SerializeField]
    private Button m_playSingleButton;
    [SerializeField]
    private Button m_playTogetherButton;
    [SerializeField]
    private NetworkManager m_networkManager;

    private LevelLoader m_levelLoader;

    private void Start()
    {
        m_levelLoader = FindObjectOfType<LevelLoader>();

        SetButtonsActive(false);
        m_networkStatus.text = string.Format(TextConstants.NetworkStatusFormat, TextConstants.SearchServer);
    }

    public void PlaySingle()
    {
        m_networkManager.RunServer();
        LoadGameplayScene();
    }

    public void PlayMultiplayer()
    {
        if(Network.connections.Length > 0)
        {
            LoadGameplayScene();
            return;
        }

        m_networkStatus.text = string.Format(TextConstants.NetworkStatusFormat, TextConstants.WaitingSecondPlayer);
    }

    [RPC]
    public void LoadGameplayScene()
    {
        m_levelLoader.LoadNetworkLevel(LevelNames.SpaceSceneName);
    }

    private void SetButtonsActive(bool isActive)
    {
        m_playTogetherButton.interactable = isActive;
    }

    private void OnServerInitialized()
    {
        SetButtonsActive(true);
        m_networkStatus.text = string.Format(TextConstants.NetworkStatusFormat, TextConstants.RunAsServer);
    }

    private void OnConnectedToServer()
    {
        SetButtonsActive(true);
        m_networkStatus.text = string.Format(TextConstants.NetworkStatusFormat, TextConstants.RunAsClient);
    }
}
