﻿using System.Linq;
using UnityEngine;

public class SpaceSceneMainScreen : BaseGuiMediator
{
    [SerializeField]
    private GUIText m_textTimer;
    [SerializeField]
    private GUIText m_textScore;
    [SerializeField]
    private SyncedTime m_syncedTime;

    private SpaceSceneModel m_spaceSceneModell;
    private bool m_isStartTimer;

    public void Init(SpaceSceneModel spaceSceneModel)
    {
        m_spaceSceneModell = spaceSceneModel;
        var minePlayerModelId = m_spaceSceneModell.PlayerModels.First(model => model.Value.IsMine).Key;
        m_spaceSceneModell.PlayerModels[minePlayerModelId].Score.Changed += OnScoreChanged;
    }

    public void StartRaceTimer()
    {
        m_isStartTimer = true;
        m_syncedTime.SetStartTime(m_spaceSceneModell.StartRaceTime);
    }

    public void StopRaceTimer()
    {
        m_isStartTimer = false;        
    }

    private void Update()
    {
        if(!m_isStartTimer)
        {
            return;
        }

        m_syncedTime.Elapse();
        m_textTimer.text = Utils.FormatTime((float) m_syncedTime.ElapsedTime);
    }

    private void OnScoreChanged(NotifableInteger.NotifableIntegerEventArgs args)
    {
        m_textScore.text = args.NewValue.ToString();
    }

    private void OnDestroy()
    {
        var minePlayerModel = m_spaceSceneModell.PlayerModels.First(model => model.Value.IsMine).Value;
        minePlayerModel.Score.Changed -= OnScoreChanged;
    }
}