﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GuiManager
{
    private Dictionary<Type, string> mediatorsMap;
    private BaseGuiMediator activeScreen;

    public GuiManager()
    {
        InitMediatorsMap();
    }

    public T ShowScreen<T>() where T : BaseGuiMediator
    {
        if(activeScreen != null)
        {
            activeScreen.Close();
        }

        activeScreen = (GameObject.Instantiate(Resources.Load(mediatorsMap[typeof ( T )])) as GameObject).GetComponent<T>();
        return (T) activeScreen;
    }

    public T ShowDialog<T>() where T : BaseGuiMediator
    {
        return (GameObject.Instantiate(Resources.Load(mediatorsMap[typeof ( T )])) as GameObject).GetComponent<T>();
    }

    private void InitMediatorsMap()
    {
        mediatorsMap = new Dictionary<Type, string>()
        {
            {typeof ( MainMenuScreen ), "GUI/Screens/MainMenuScreen"},
            {typeof ( StartTimeDialog ), "GUI/Dialogs/StartTimeDialog"},
            {typeof ( EndGameDialog ), "GUI/Dialogs/EndGameDialog"},
            {typeof ( SpaceSceneMainScreen ), "GUI/Screens/SpaceSceneScreen"}
        };
    }
}