﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class EndGameDialog : BaseGuiMediator
{
    [SerializeField]
    private Text m_winnerLabel;
    [SerializeField]
    private Text m_mineScores;
    [SerializeField]
    private Text m_mineTime;
    [SerializeField]
    private Text m_enemyScores;
    [SerializeField]
    private Text m_enemyTime;

    public event Action LoadMainMenuClickedEvent;

    public virtual void Init(SpaceSceneModel sceneModel)
    {
        SetWinnerLabel(sceneModel);
        SetMineResults(sceneModel);

        if(sceneModel.PlayerModels.Count > 1)
        {
            SetEnemyResults(sceneModel);
        }
    }

    private void SetWinnerLabel(SpaceSceneModel sceneModel)
    {

        double minFinishTime = sceneModel.PlayerModels.Min(player => player.Value.FinishTime);
        var winner = sceneModel.PlayerModels.FirstOrDefault(player => player.Value.FinishTime == minFinishTime).Value;

        if(winner.IsMine)
        {
            m_winnerLabel.text = TextConstants.MineVictoryLabel;
        }
        else
        {
            m_winnerLabel.text = TextConstants.EnemyVictoryLabel;
        }
    }

    private void SetMineResults(SpaceSceneModel sceneModel)
    {
        var mineModel = sceneModel.PlayerModels.First(player => player.Value.IsMine).Value;
        m_mineScores.text = string.Format(TextConstants.MineScoreFormat, mineModel.Score.Value);
        m_mineTime.text = string.Format(TextConstants.MineTimeFormat, Utils.FormatTime((float) mineModel.FinishTime));
    }

    private void SetEnemyResults(SpaceSceneModel sceneModel)
    {
        var enemyModel = sceneModel.PlayerModels.First(player => !player.Value.IsMine).Value;
        m_enemyScores.text = string.Format(TextConstants.EnemyScoreFormat, enemyModel.Score.Value);
        if(enemyModel.FinishTime != double.MaxValue)
        {
            m_enemyTime.text = string.Format(TextConstants.EnemyTimeFormat, Utils.FormatTime((float) enemyModel.FinishTime));
        }
    }

    public void OnLoadMainMenuClicked()
    {
        if(LoadMainMenuClickedEvent != null)
        {
            LoadMainMenuClickedEvent();
        }
    }
}