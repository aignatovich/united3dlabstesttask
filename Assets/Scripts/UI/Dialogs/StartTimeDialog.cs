﻿using UnityEngine;

public class StartTimeDialog : BaseGuiMediator
{
    [SerializeField]
    private GUIText m_guiTimeText;

    private double m_startRaceTime;

    public void Init(double startRaceTime)
    {
        m_startRaceTime = startRaceTime;
    }

    private void Update()
    {
        m_guiTimeText.text = ((int) (m_startRaceTime - Network.time )).ToString();
        if(Network.time > m_startRaceTime)
        {
            Close();
        }
    }
}