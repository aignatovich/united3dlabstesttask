﻿using UnityEngine;

public class BaseGuiMediator : MonoBehaviour
{
    public virtual void Init()
    {

    }

    public virtual void Close()
    {
        Destroy(gameObject);
    }
}