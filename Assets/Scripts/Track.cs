﻿using UnityEngine;
using System.Collections;

public class Track : MonoBehaviour
{
    [SerializeField]
    private RoadSpline m_raceLine;
    [SerializeField]
    private RoadSpline m_borderLeft;
    [SerializeField]
    private RoadSpline m_borderRight;
	[SerializeField]
	private float m_offsetFromEnd = 0;
	[SerializeField]
	private float m_borderWidth = 0.25f;
    
    public float length
    {
        get { return m_raceLine.Length; }
    }
	
	public float offsetFromEnd
	{
		get { return m_offsetFromEnd; }
	}
	
	public float endDistance
	{
		get { return length - offsetFromEnd; }	
	}

    public Vector3 GetTrackPosition(Vector3 worldPosition)
    {
        Vector3 trackPosition;

        float distanceToSpline;
        m_raceLine.SamplePoint(worldPosition, out trackPosition.z, out distanceToSpline);

        Vector3 basis_cnt, basis_right, basis_up, basis_front;
        GetBasisAtDistance(trackPosition.z, 1f, out basis_cnt, out basis_right, out basis_up, out basis_front);

        Vector3 offset = worldPosition - basis_cnt;
        trackPosition.x = Vector3.Dot(offset, basis_right);
        trackPosition.y = Vector3.Dot(offset, basis_up);

        return trackPosition;
    }

    public Vector3 BorderClampPosition(Vector3 trackPosition, out bool leftClamp, out bool rightClamp, out bool heavy)
    {
        Matrix4x4 mtx1 = new Matrix4x4();
        int idx = -1;

        Vector3 cnt;
        m_raceLine.GetPointAtDist(trackPosition.z, ref mtx1, ref idx, out cnt);

        idx = -1;

        float distOnSpline, distToLeftBorder, distToRightBorder;
        m_borderLeft.SamplePoint(cnt, out distOnSpline, out distToLeftBorder);
        m_borderRight.SamplePoint(cnt, out distOnSpline, out distToRightBorder);

        distToLeftBorder -= m_borderWidth;
        distToRightBorder -= m_borderWidth;
		
		leftClamp = (trackPosition.x <= -(distToLeftBorder - 0.1f));
		rightClamp = (trackPosition.x >= (distToRightBorder - 0.1f));
		
		heavy = (trackPosition.x <= -(distToLeftBorder - 0.2f)) || (trackPosition.x >= (distToRightBorder - 0.2f));

        trackPosition.x = Mathf.Clamp(trackPosition.x, -distToLeftBorder, distToRightBorder);

        return trackPosition;
    }

    public void GetBasisAtDistance(float dist, float front_offset, out Vector3 cnt, out Vector3 right, out Vector3 up, out Vector3 front)
    {
        Matrix4x4 mtx1 = new Matrix4x4();
        int idx = -1;

        m_raceLine.GetPointAtDist(dist, ref mtx1, ref idx, out cnt);

        dist += front_offset;

        if(dist >= m_raceLine.Length)
        {
            dist -= m_raceLine.Length;
            idx = -1;
        }

        m_raceLine.GetPointAtDist(dist, ref mtx1, ref idx, out front);

        front = front - cnt;
        front.Normalize();

        float distOnSpline, distToSpline;
        m_borderRight.SamplePoint(cnt, out distOnSpline, out distToSpline);

        idx = -1;

        Vector3 posOnBorderRight;
        m_borderRight.GetPointAtDist(distOnSpline, ref mtx1, ref idx, out posOnBorderRight);

        right = (posOnBorderRight - cnt).normalized;
        up = Vector3.Cross(front, right);
    }

    public Vector3 GetAccelerationAtDistance(float dist)
    {
        Vector3 p, v, a;
        Matrix4x4 mtx1 = new Matrix4x4();
        Matrix4x4 mtx2 = new Matrix4x4();
        Matrix4x4 mtx3 = new Matrix4x4();
        int idx = -1;
        m_raceLine.GetPointAtDist(dist, ref mtx1, ref mtx2, ref mtx3, ref idx, out p, out v, out a);
        return a;
    }
	
	public float RepeatDistance(float distance)
	{
		return Mathf.Repeat(distance, length);
	}
	
	void OnDrawGizmosSelected()
    {
        Matrix4x4 cmtx1 = new Matrix4x4();
        int cidx = -1;

        Vector3 pnt;
		
		m_raceLine.GetPointAtDist(endDistance, ref cmtx1, ref cidx, out pnt);

        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(pnt, 3f);
    }
}