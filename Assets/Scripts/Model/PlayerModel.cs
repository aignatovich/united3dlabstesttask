﻿public class PlayerModel
{
    public string ModelId { get; set; }
    public bool IsMine { get; set; }
    public double FinishTime { get; set; }
    public NotifableInteger Score { get; set; }

    public PlayerModel(string modelId, bool isMine, double finishTime, int collectedBonuses)
    {
        ModelId = modelId;
        IsMine = isMine;
        FinishTime = finishTime;
        Score = new NotifableInteger(collectedBonuses);
    }
}
