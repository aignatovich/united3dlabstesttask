﻿using System;
using UnityEngine;

public class NotifableInteger
{
    public Action<NotifableIntegerEventArgs> Changed;

    private int m_currentValue;

    public int Value
    {
        get
        {
            return m_currentValue;
        }

        set
        {
            int oldValue = m_currentValue;
            m_currentValue = value;
            OnChanged(new NotifableIntegerEventArgs(oldValue, m_currentValue));
        }
    }

    public NotifableInteger(int value)
    {
        Value = value;
    }

    public static implicit operator NotifableInteger(int value)
    {
        return new NotifableInteger(value);
    }

    public static implicit operator int(NotifableInteger currentInstance)
    {
        return currentInstance.Value;
    }

    protected virtual void OnChanged(NotifableIntegerEventArgs e)
    {
        if(Changed != null)
        {
            Changed(e);
        }
    }

    #region EventArgs

    public class NotifableIntegerEventArgs : EventArgs
    {
        public int OldValue { get; private set; }
        public int NewValue { get; private set; }

        public NotifableIntegerEventArgs(int oldValue, int newValue)
        {
            OldValue = oldValue;
            NewValue = newValue;
        }
    }

    #endregion
}