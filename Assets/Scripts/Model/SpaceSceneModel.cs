﻿using System.Collections.Generic;

public class SpaceSceneModel
{
    public Dictionary<string, PlayerModel> PlayerModels { get; private set; }
    public double StartRaceTime { get; set; }

    private int lastModelId;
    private string modelIdFormat = "ModelId_{0}";

    public SpaceSceneModel()
    {
        StartRaceTime = double.MaxValue;
        PlayerModels = new Dictionary<string, PlayerModel>();
    }

    public string AddPlayerModel(bool isMine)
    {
        var newModelId = string.Format(modelIdFormat, ++lastModelId);
        //Todo float.MaxValue - временно, итак долго делаю
        PlayerModels.Add(newModelId, new PlayerModel(newModelId,isMine, double.MaxValue, 0));
        return newModelId;
    }
}
