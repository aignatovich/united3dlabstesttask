﻿using UnityEngine;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class Server 
{
    private UdpClient m_udpClient;
    private IPEndPoint m_remoteEnd;
    private readonly MonoBehaviour m_coroutineMonoBehavior;

    public Server(MonoBehaviour coroutineMonoBehavior)
    {
        m_coroutineMonoBehavior = coroutineMonoBehavior;
    }

    public NetworkConnectionError StartGameServer()
    {
        Debug.Log(string.Format(TextConstants.StartServerFormat, Network.player.ipAddress));
        var initStatus = Network.InitializeServer(NetworkConstants.MaxConnections, NetworkConstants.ServerPort, false);
        if(initStatus == NetworkConnectionError.NoError)
        {
            m_coroutineMonoBehavior.StartCoroutine(StartBroadcast());
        }

        return initStatus;
    }

    private IEnumerator StartBroadcast()
    {
        Debug.Log(TextConstants.StartBroadCast);
        m_udpClient = new UdpClient();
        m_udpClient.JoinMulticastGroup(NetworkConstants.GroupAddress);
        m_remoteEnd = new IPEndPoint(NetworkConstants.GroupAddress, NetworkConstants.BottomClientPort);

        while (true)
        {
            var buffer = Encoding.ASCII.GetBytes(TextConstants.GameServer);
            m_udpClient.Send(buffer, buffer.Length, m_remoteEnd);
            yield return new WaitForSeconds(1);
        }
    }
}
