﻿using System;
using UnityEngine;
using System.Net;
using System.Net.Sockets;

public class ServerDetector : MonoBehaviour
{
    public event Action<IPAddress> SearchServerSuccessEvent;
    public event Action SearchServerFailedEvent;

    private UdpClient m_udpClient;
    private IPEndPoint m_remoteEnd;
    private IPAddress m_serverip;
    private TimeCounter m_connectionTimer;
    private bool m_isSearchInProgress;

    public void StartSearchServer(float timeToSearch)
    {
        m_remoteEnd = GetAvailableEndPoint(); 
        m_udpClient = new UdpClient(m_remoteEnd);
        m_udpClient.JoinMulticastGroup(NetworkConstants.GroupAddress);
        m_udpClient.BeginReceive(ServerLookup, null);

        m_connectionTimer = new TimeCounter(timeToSearch);
        m_isSearchInProgress = true;
    }

    private IPEndPoint GetAvailableEndPoint()
    {
        for (int i = NetworkConstants.BottomClientPort; i <= NetworkConstants.TopClientPort; i++)
        {
            //Todo костыль временный, переделать попозже
            try
            {
                var remoteEnd = new IPEndPoint(IPAddress.Any, i);
                var udpClient = new UdpClient(remoteEnd);
                udpClient.Close();
                return remoteEnd;
            }
            catch ( SocketException )
            {
                Debug.LogError("port " + i + " is busy");
            }
        }

        Debug.LogError("ServerDetector.GetAvailableEndPoint: can't find available port ");
        return null;
    }

    private void Update()
    {
        if(!m_isSearchInProgress)
        {
            return;
        }

        m_connectionTimer.Elapse(Time.deltaTime);
        if(m_connectionTimer.IsElapsed)
        {
            EndSearchServer();
            if(SearchServerFailedEvent != null)
            {
                m_isSearchInProgress = false;
                SearchServerFailedEvent();
            }
        }
    }

    private void ServerLookup(IAsyncResult asyncResult)
    {
        Debug.Log("ServerLookup " + asyncResult.AsyncState);

        m_udpClient.EndReceive(asyncResult, ref m_remoteEnd);
        m_serverip = m_remoteEnd.Address;

        if(m_serverip != null && SearchServerSuccessEvent != null)
        {
            Debug.Log("Server found with ip: " + m_serverip);
            EndSearchServer();
            m_isSearchInProgress = false;
            SearchServerSuccessEvent(m_serverip);
        }
    }

    public void EndSearchServer()
    {
        Debug.Log("Stop search server");
        m_isSearchInProgress = false;
        m_udpClient.Close();
    }
}