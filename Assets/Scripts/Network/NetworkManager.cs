﻿using System.Net;
using UnityEngine;

//Todo пересмотреть функционал и переделать получше
public class NetworkManager : MonoBehaviour
{
    [SerializeField]
    private ServerDetector m_serverDetector;

    private Server m_server;
    private IPAddress m_serverIp;
    private bool m_isTryConnectToServer;

    private void Awake()
    {
        Network.Disconnect();
        m_serverDetector.StartSearchServer(NetworkConstants.SeachServerTime);
        m_serverDetector.SearchServerSuccessEvent += OnSearchServerSuccess;
        m_serverDetector.SearchServerFailedEvent += OnSearchServerFailed;
    }

    private void OnSearchServerFailed()
    {
        RunServer();
    }

    void Update()
    {
        if (m_isTryConnectToServer)
        {
            m_isTryConnectToServer = false;
            Network.Connect(m_serverIp.ToString(), NetworkConstants.ServerPort);
        }
    }

    public void RunServer()
    {
        if(Network.isServer)
        {
            return;
        }

        if(Network.isClient)
        {
            Network.Disconnect();
        }

        m_serverDetector.EndSearchServer();
        m_server = new Server(this);
        if(m_server.StartGameServer() != NetworkConnectionError.NoError)
        {
            m_serverDetector.StartSearchServer(NetworkConstants.SeachServerTime);
        }
    }

    private void OnSearchServerSuccess(IPAddress ipAddress)
    {
        m_serverIp = ipAddress;
        m_isTryConnectToServer = true;
    }

    private void OnDestroy()
    {
        m_serverDetector.EndSearchServer();
    }
}