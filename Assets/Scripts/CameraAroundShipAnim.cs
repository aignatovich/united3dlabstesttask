using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animation))]
public class CameraAroundShipAnim : MonoBehaviour
{	
	[SerializeField]
	private string m_tag;
	private CameraFollowShip m_followController;
	private Transform m_target;
	private Camera m_camera = null;
	
	public bool isPlaying
	{
		get { return animation.isPlaying; }
	}
	
	public void SetTarget(Transform target)
	{
		m_target = target;
		
		foreach(Transform t in m_target.GetComponentsInChildren<Transform>())
        {
            if(t.CompareTag(m_tag))
            {
                m_target = t;
                break;
            }
        }
	}
	
	public void PlayAnim(bool start)
	{
		StartCoroutine(DoAnim(start ? "start" : "finish"));
	}
	
	void Awake()
	{
		m_followController = GetComponent<CameraFollowShip>();
		m_camera = GetComponentInChildren<Camera>();
	}
	
	private IEnumerator DoAnim(string animName)
	{
		OnStartAnim();
		
		animation.wrapMode = WrapMode.Once;
		if(animation.GetClip(animName) != null)
			animation.Play(animName);
			
		while(animation.isPlaying)
		{
			yield return null;
		}
		
		OnEndAnim();
	}
	
	private void OnStartAnim()
	{
		if(m_followController) m_followController.enabled = false;
		
		if(m_target)
		{
			transform.parent = m_target;
			transform.localPosition = Vector3.zero;
			transform.localRotation = Quaternion.identity;
		}
		
		if(m_camera)
		{
			m_camera.transform.localRotation = Quaternion.Euler(0, -90, 0);
		}
	}
	
	private void OnEndAnim()
	{
		if(m_followController) m_followController.enabled = true;
		transform.parent = null;
		
		if(m_camera)
		{
			m_camera.transform.localRotation = Quaternion.identity;
		}
	}
}