﻿using UnityEngine;

//Todo костыль временный чтобы, вместе со стартовой сценой, чтобы по быстрому решить проблему с размножением LevelLoader и NetworkManager
public class StartSceneManager : MonoBehaviour
{
    private void Start()
    {
        Application.LoadLevel(LevelNames.MainMenuSceneName);
    }
}