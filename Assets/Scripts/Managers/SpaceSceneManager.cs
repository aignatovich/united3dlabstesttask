﻿using UnityEngine;

public class SpaceSceneManager : MonoBehaviour
{
    [SerializeField]
    private ShipsManager m_shipsManager;
    [SerializeField]
    private NetworkView m_networkView;

    private GuiManager m_guiManager;
    private SpaceSceneModel m_spaceSceneModel;
    private SpaceSceneMainScreen m_spaceSceneMainScreen;
    private StartTimeDialog m_startTimeDialog;
    private EndGameDialog m_endGameDialog;

    private bool m_isRaceStarted;

    private void OnNetworkLoadedLevel()
    {
        m_spaceSceneModel = new SpaceSceneModel();
        m_guiManager = new GuiManager();
        m_shipsManager.SpawnShips(m_spaceSceneModel);
        m_shipsManager.ShipFinishedEvent += OnShipFinshed;
        m_spaceSceneModel.StartRaceTime = double.MaxValue;
        m_spaceSceneMainScreen = m_guiManager.ShowScreen<SpaceSceneMainScreen>();
        m_spaceSceneMainScreen.Init(m_spaceSceneModel);

        if(Network.isServer)
        {
            var startRaceTime = Network.time + Constants.StartRaceIdleTime;
            m_networkView.RPC("SetRaceStartTime", RPCMode.Others, (startRaceTime).ToString());
            SetRaceStartTime((startRaceTime).ToString());
        }
    }

    private void OnShipFinshed(string shipModelId)
    {
        if(m_spaceSceneModel.PlayerModels[shipModelId].IsMine)
        {
            m_endGameDialog = m_guiManager.ShowDialog<EndGameDialog>();
            m_endGameDialog.Init(m_spaceSceneModel);
            m_endGameDialog.LoadMainMenuClickedEvent += LoadMainMenu;
            m_spaceSceneMainScreen.StopRaceTimer();
        }
    }

    private void LoadMainMenu()
    {
        Application.LoadLevel(LevelNames.MainMenuSceneName);
    }

    private void Update()
    {
        //Todo переделать
        if(!m_isRaceStarted && m_spaceSceneModel != null && Network.time > m_spaceSceneModel.StartRaceTime)
        {
            m_isRaceStarted = true;
            m_shipsManager.StartMoveShips();
            m_spaceSceneMainScreen.StartRaceTimer();
        }
    }

    [RPC]
    public void SetRaceStartTime(string time)
    {
        m_spaceSceneModel.StartRaceTime = double.Parse(time);
        m_startTimeDialog = m_guiManager.ShowDialog<StartTimeDialog>();
        m_startTimeDialog.Init(m_spaceSceneModel.StartRaceTime);
    }

    public void OnDestroy()
    {
        m_shipsManager.ShipFinishedEvent -= OnShipFinshed;        
    }
}