﻿using System;
using UnityEngine;

public class Bonus : MonoBehaviour
{
    [SerializeField] private float m_speedBonus;
    [SerializeField] private int m_score;

    private void OnTriggerEnter(Collider coll)
    {
        if(!Network.isServer)
        {
            return;
        }

        if(coll.gameObject.layer != LayerMask.NameToLayer(Layers.ShipLayerName))
        {
            return;
        }

        var shipNetworkView = coll.gameObject.GetComponent<NetworkView>();

        if(shipNetworkView == null)
        {
            throw new Exception("Bonus.OnTriggerEnter: ship is null");
        }

        shipNetworkView.RPC("AddBonusSpeed", RPCMode.AllBuffered, m_speedBonus, m_score, Network.time.ToString());
        Network.Destroy(gameObject);
    }
}