﻿using UnityEngine;
using System.Collections;

public class RoadSpline : MonoBehaviour
{
    [System.Serializable]
    class PointData
    {
        public Vector3 vel;
        public float dist;
    }

    [SerializeField]
    private Vector3[] m_Points = new Vector3[0];
    [SerializeField]
    private PointData[] m_Data = new PointData[0];
    [SerializeField]
    private float m_Length;
    private static Matrix4x4 s_PosMtx = GetPosMtx();
    private static Matrix4x4 s_VelMtx = GetVelMtx();
    private static Matrix4x4 s_AccMtx = GetAccMtx();

    public float Length
    {
        get { return m_Length; }
    }

    public int PointCount
    {
        get { return m_Points.Length; }
    }

    public Vector3[] Points
    {
        get { return m_Points; }
        set
        {
            m_Points = value;
            Rebuild();
        }
    }

    public Vector3 GetPointAt(int index)
    {
        if(index < 0 || index >= m_Points.Length)
            return Vector3.zero;

        return m_Points[index];
    }

    public void SetPointAt(int index, Vector3 point)
    {
        if(index < 0 || index >= m_Points.Length)
            return;

        m_Points[index] = point;
        Rebuild();
    }

    public Vector3 GetVelAt(int index)
    {
        if(index < 0 || index >= m_Data.Length)
            return Vector3.zero;

        return m_Data[index].vel;
    }

    public void SetVelAt(int index, Vector3 vel)
    {
        if(index < 0 || index >= m_Data.Length)
            return;

        m_Data[index].vel = vel;
    }

    public bool SamplePoint(Vector3 point, out float distOnSpline, out float distToSpline)
    {
        bool over = false;
        Vector3 p0, p1;
        float min_t = 0;
        int min_i = 0;
        float sqrdist, min_sqrdist = float.MaxValue;

        for(int i = 0; i < m_Points.Length - 1; ++i)
        {
            p0 = m_Points[i];
            p1 = m_Points[i + 1];

            Vector3 n = p1 - p0;
            float d = -Vector3.Dot(n, point);

            float div = n.sqrMagnitude;
            float t = -(d + Vector3.Dot(n, p0)) / div;

            if(t < 0 || t > 1)
                continue;

            Vector3 np = p0 * (1 - t) + p1 * t;
            sqrdist = Vector3.SqrMagnitude(np - point);

            if(sqrdist < min_sqrdist)
            {
                min_i = i;
                min_sqrdist = sqrdist;
                min_t = t;
            }
        }

        p0 = m_Points[0];
        sqrdist = Vector3.SqrMagnitude(p0 - point);

        if(sqrdist < min_sqrdist)
        {
            min_i = 0;
            min_sqrdist = sqrdist;
            min_t = 0;
        }

        for(int i = 0; i < m_Points.Length - 1; ++i)
        {
            p1 = m_Points[i + 1];
            sqrdist = Vector3.SqrMagnitude(p1 - point);

            if(sqrdist < min_sqrdist)
            {
                min_i = i;
                min_sqrdist = sqrdist;
                min_t = 1;

                if(i == m_Points.Length - 2)
                    over = true;
            }
        }

        distOnSpline = m_Data[min_i].dist;
        distOnSpline += (m_Data[min_i + 1].dist - m_Data[min_i].dist) * min_t;
        distToSpline = Mathf.Sqrt(min_sqrdist);
        return over;
    }

    public bool SamplePoint(Vector3 point, out Vector3 pos, out Vector3 dir)
    {
        bool over = false;
        Vector3 p0, p1;
        float min_t = 0f;
        int min_i = 0;
        float sqrdist, min_sqrdist = float.MaxValue;

        for(int i = 0; i < m_Points.Length - 1; ++i)
        {
            p0 = m_Points[i];
            p1 = m_Points[i + 1];

            Vector3 n = p1 - p0;
            float d = -Vector3.Dot(n, point);

            float div = n.sqrMagnitude;
            float t = -(d + Vector3.Dot(n, p0)) / div;

            if(t < 0 || t > 1)
                continue;

            Vector3 np = p0 * (1 - t) + p1 * t;
            sqrdist = Vector3.SqrMagnitude(np - point);

            if(sqrdist < min_sqrdist)
            {
                min_i = i;
                min_sqrdist = sqrdist;
                min_t = t;
            }
        }

        p0 = m_Points[0];
        sqrdist = Vector3.SqrMagnitude(p0 - point);

        if(sqrdist < min_sqrdist)
        {
            min_i = 0;
            min_sqrdist = sqrdist;
            min_t = 0;
        }

        for(int i = 0; i < m_Points.Length - 1; ++i)
        {
            p1 = m_Points[i + 1];
            sqrdist = Vector3.SqrMagnitude(p1 - point);

            if(sqrdist < min_sqrdist)
            {
                min_i = i;
                min_sqrdist = sqrdist;
                min_t = 1;

                if(i == m_Points.Length - 2)
                    over = true;
            }
        }

        dir = m_Points[min_i + 1] - m_Points[min_i];
        pos = m_Points[min_i] + dir * min_t;
        dir.Normalize();
        return over;
    }

    public void GetPointAtDist(float dist, ref Matrix4x4 mtx1, ref Matrix4x4 mtx2, ref Matrix4x4 mtx3, ref int idx, out Vector3 pos, out Vector3 vel, out float rad)
    {
        Vector3 acc;
        GetPointAtDist(dist, ref mtx1, ref mtx2, ref mtx3, ref idx, out pos, out vel, out acc);
        rad = CalcRadius(vel, acc);
    }

    public void GetPointAtDist(float dist, ref Matrix4x4 mtx1, ref Matrix4x4 mtx2, ref Matrix4x4 mtx3, ref int idx, out Vector3 pos, out Vector3 vel, out Vector3 acc)
    {
        int i = 0;

        while(m_Data[i].dist < dist || i == 0)
            ++i;

        PointData d1 = m_Data[i - 1];
        PointData d2 = m_Data[i];

        float t = dist - d1.dist;
        t /= d2.dist - d1.dist;

        if(i != idx)
        {
            idx = i;

            Vector3 p = m_Points[i - 1];
            mtx1.m00 = p.x;
            mtx1.m10 = p.y;
            mtx1.m20 = p.z;

            p = m_Points[i];
            mtx1.m01 = p.x;
            mtx1.m11 = p.y;
            mtx1.m21 = p.z;

            float seglen = d2.dist - d1.dist;
            p = d1.vel * seglen;
            mtx1.m02 = p.x;
            mtx1.m12 = p.y;
            mtx1.m22 = p.z;

            p = d2.vel * seglen;
            mtx1.m03 = p.x;
            mtx1.m13 = p.y;
            mtx1.m23 = p.z;

            mtx3 = mtx1 * s_AccMtx;
            mtx2 = mtx1 * s_VelMtx;
            mtx1 = mtx1 * s_PosMtx;
        }

        Vector3 v = new Vector3(t * t * t, t * t, t);
        pos = mtx1.MultiplyPoint3x4(v);
        vel = mtx2.MultiplyPoint3x4(v);
        acc = mtx3.MultiplyPoint3x4(v);
    }

    public void GetPointAtDist(float dist, ref Matrix4x4 mtx1, ref int idx, out Vector3 pos)
    {
        int i = 0;

        while(m_Data[i].dist < dist || i == 0)
            ++i;

        PointData d1 = m_Data[i - 1];
        PointData d2 = m_Data[i];

        float t = dist - d1.dist;
        t /= d2.dist - d1.dist;

        if(i != idx)
        {
            idx = i;

            Vector3 p = m_Points[i - 1];
            mtx1.m00 = p.x;
            mtx1.m10 = p.y;
            mtx1.m20 = p.z;

            p = m_Points[i];
            mtx1.m01 = p.x;
            mtx1.m11 = p.y;
            mtx1.m21 = p.z;

            float seglen = d2.dist - d1.dist;
            p = d1.vel * seglen;
            mtx1.m02 = p.x;
            mtx1.m12 = p.y;
            mtx1.m22 = p.z;

            p = d2.vel * seglen;
            mtx1.m03 = p.x;
            mtx1.m13 = p.y;
            mtx1.m23 = p.z;

            mtx1 = mtx1 * s_PosMtx;
        }

        Vector3 v = new Vector3(t * t * t, t * t, t);
        pos = mtx1.MultiplyPoint3x4(v);
    }

    public float GetRadiusAtDist(float dist, ref Matrix4x4 mtx1, ref Matrix4x4 mtx2, ref int idx)
    {
        int i = 0;

        while(m_Data[i].dist < dist || i == 0)
            ++i;

        PointData d1 = m_Data[i - 1];
        PointData d2 = m_Data[i];

        float t = dist - d1.dist;
        t /= d2.dist - d1.dist;

        if(i != idx)
        {
            idx = i;

            Vector3 p = m_Points[i - 1];
            mtx1.m00 = p.x;
            mtx1.m10 = p.y;
            mtx1.m20 = p.z;

            p = m_Points[i];
            mtx1.m01 = p.x;
            mtx1.m11 = p.y;
            mtx1.m21 = p.z;

            float seglen = d2.dist - d1.dist;
            p = d1.vel * seglen;
            mtx1.m02 = p.x;
            mtx1.m12 = p.y;
            mtx1.m22 = p.z;

            p = d2.vel * seglen;
            mtx1.m03 = p.x;
            mtx1.m13 = p.y;
            mtx1.m23 = p.z;

            mtx2 = mtx1 * s_VelMtx;
            mtx1 = mtx1 * s_AccMtx;
        }

        Vector3 v = new Vector3(t * t * t, t * t, t);
        return CalcRadius(mtx2.MultiplyPoint3x4(v), mtx1.MultiplyPoint3x4(v));
    }

    public void Rebuild()
    {
        m_Data = new PointData[m_Points.Length];

        float dist = 0;

        for(int i = 0; i < m_Data.Length; ++i)
        {
            m_Data[i] = new PointData();

            if(i != 0)
            {
                dist += Vector3.Distance(m_Points[i - 1], m_Points[i]);
                m_Data[i].dist = dist;
            }
        }

        for(int i = 1; i < m_Data.Length - 1; ++i)
        {
            Vector3 v1 = Vector3.Normalize(m_Points[i + 1] - m_Points[i]);
            Vector3 v2 = Vector3.Normalize(m_Points[i - 1] - m_Points[i]);
            m_Data[i].vel = Vector3.Normalize(v1 - v2);
        }

        if(m_Data.Length > 0)
        {
            m_Data[0].dist = 0;
            m_Data[0].vel = GetStartVel(0);
            m_Data[m_Data.Length - 1].vel = GetEndVel(m_Data.Length - 1);
        }

        m_Length = m_Data.Length > 0 ? m_Data[m_Data.Length - 1].dist : 0;
		
		for(int i = 0; i < 5; ++i)
			Smooth();
    }
	
	public void Smooth()
	{
		if(m_Points.Length < 3)
			return;
		
		Vector3 oldVel = GetStartVel(0);
		
		for(int i = 1; i < m_Points.Length - 1; ++i)
		{
			Vector3 newVel = GetEndVel(i) * m_Data[i].dist + GetStartVel(i) * m_Data[i-1].dist;
			newVel = newVel * (1f / (m_Data[i-1].dist + m_Data[i].dist));
			m_Data[i-1].vel = oldVel;
			oldVel = newVel;
		}
		
		m_Data[m_Data.Length - 1].vel = GetEndVel(m_Data.Length - 1);
		m_Data[m_Data.Length - 2].vel = oldVel;
	}

    private Vector3 GetStartVel(int index)
    {
        if(index >= m_Points.Length - 1 || index < 0)
            return Vector3.zero;

        Vector3 t = 3 * (m_Points[index + 1] - m_Points[index]) / (m_Data[index + 1].dist - m_Data[index].dist);
        return (t - m_Data[index + 1].vel) * 0.5f;
    }

    private Vector3 GetEndVel(int index)
    {
        if(index >= m_Points.Length || index < 1)
            return Vector3.zero;

        Vector3 t = 3 * (m_Points[index] - m_Points[index - 1]) / (m_Data[index].dist - m_Data[index - 1].dist);
        return (t - m_Data[index - 1].vel) * 0.5f;
    }

    private static float CalcRadius(Vector3 vel, Vector3 acc)
    {
        float rad = vel.sqrMagnitude / acc.magnitude;

        if(Vector3.Cross(vel, acc).y < 0)
            rad = -rad;

        return rad;
    }

    private static Matrix4x4 GetPosMtx()
    {
        Matrix4x4 m = new Matrix4x4();
        m.SetRow(0, new Vector4(2, -3, 0, 1));
        m.SetRow(1, new Vector4(-2, 3, 0, 0));
        m.SetRow(2, new Vector4(1, -2, 1, 0));
        m.SetRow(3, new Vector4(1, -1, 0, 0));
        return m;
    }

    private static Matrix4x4 GetVelMtx()
    {
        Matrix4x4 m = new Matrix4x4();
        m.SetRow(0, new Vector4(0, 6, -6, 0));
        m.SetRow(1, new Vector4(0, -6, 6, 0));
        m.SetRow(2, new Vector4(0, 3, -4, 1));
        m.SetRow(3, new Vector4(0, 3, 1, 0));
        return m;
    }

    private static Matrix4x4 GetAccMtx()
    {
        Matrix4x4 m = new Matrix4x4();
        m.SetRow(0, new Vector4(0, 0, 12, -6));
        m.SetRow(1, new Vector4(0, 0, -12, 6));
        m.SetRow(2, new Vector4(0, 0, 6, -4));
        m.SetRow(3, new Vector4(0, 0, 6, -2));
        return m;
    }

    void OnDrawGizmosSelected()
    {
        if(m_Points.Length < 2)
            return;

        Matrix4x4 cmtx1 = new Matrix4x4();
        int cidx = -1;

        Vector3 pnt;
        Vector3 oldpnt = m_Points[0];

        float dist = 0f;
        float length = Length;

        while(dist < length)
        {
            GetPointAtDist(dist, ref cmtx1, ref cidx, out pnt);

            Gizmos.color = Color.Lerp(Color.green, Color.red, dist / length);
            Gizmos.DrawLine(oldpnt, pnt);

            dist += 1f;
            oldpnt = pnt;
        }
    }
}