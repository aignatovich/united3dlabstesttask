﻿using UnityEngine;

public static class Utils
{
    public static string FormatTime(float time)
    {
        int minutes = (int) Mathf.Floor(time / 60.0f);
        int seconds = (int) Mathf.Floor(time - minutes * 60.0f);

        float milliseconds = time - Mathf.Floor(time);
        milliseconds = Mathf.Floor(milliseconds * 1000.0f);

        string sMinutes = "00" + minutes;
        sMinutes = sMinutes.Substring(sMinutes.Length - 2);

        string sSeconds = "00" + seconds;
        sSeconds = sSeconds.Substring(sSeconds.Length - 2);

        string sMilliseconds = "000" + milliseconds;
        sMilliseconds = sMilliseconds.Substring(sMilliseconds.Length - 3);

        return sMinutes + ":" + sSeconds + ":" + sMilliseconds;
    }
}