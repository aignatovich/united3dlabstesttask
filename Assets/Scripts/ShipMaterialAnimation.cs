using UnityEngine;
using System.Collections;

public class ShipMaterialAnimation : MonoBehaviour
{
	[SerializeField]
	private float m_lerpSpeed = 0.02f;
	
	private Color m_currentColor;
	private float m_lerpT = 0;
	private Material m_material;
	
	void Start()
	{
		m_material = renderer.material;
		m_currentColor = m_material.GetColor("_RimColor");
	}
	
	void Update()
	{
		m_lerpT = Mathf.MoveTowards(m_lerpT, 0, m_lerpSpeed);
		m_currentColor = Color.Lerp(Color.black, m_currentColor, m_lerpT);
		m_material.SetColor("_RimColor", m_currentColor);
	}
	
	private void OnApplyBonus(Color shipColor)
	{
		m_lerpT = 1f;
		m_currentColor = shipColor;
	}
}