﻿using UnityEngine;
using System.Collections;

public class LevelLoader : MonoBehaviour
{
    [SerializeField]
    private NetworkView m_networkView;
    
    private int m_lastLevelPrefix = 0;

    private void Awake()
    {
        m_networkView.group = 1;
    }

    public void LoadNetworkLevel(string level)
    {
        Network.RemoveRPCsInGroup(0);
        Network.RemoveRPCsInGroup(1);

        m_networkView.RPC("LoadLevel", RPCMode.AllBuffered, level, m_lastLevelPrefix + 1);
    }

    [RPC]
    private IEnumerator LoadLevel(string level, int levelPrefix)
    {
        m_lastLevelPrefix = levelPrefix;

        Network.SetSendingEnabled(0, false);
        Network.isMessageQueueRunning = false;

        Network.SetLevelPrefix(levelPrefix);
        Application.LoadLevel(level);
        yield return null;

        Network.isMessageQueueRunning = true;
        Network.SetSendingEnabled(0, true);

        var gameObjects = FindObjectsOfType<GameObject>();
        foreach (var go in gameObjects)
            go.SendMessage("OnNetworkLoadedLevel", SendMessageOptions.DontRequireReceiver);
    }

    private void OnDisconnectedFromServer()
    {
        Application.LoadLevel(LevelNames.MainMenuSceneName);
    }
}