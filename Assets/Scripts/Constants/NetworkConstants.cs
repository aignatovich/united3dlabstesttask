﻿using System.Net;

public static class NetworkConstants
{
    static NetworkConstants()
    {
        GroupAddress = IPAddress.Parse("224.0.0.224");
    }
    public static readonly IPAddress GroupAddress;

    public const int ServerPort = 5000;
    public const int BottomClientPort = 5100;
    public const int TopClientPort = 5110;
    public const int MaxConnections = 10;
    public const float SeachServerTime = 5.0f;
}
