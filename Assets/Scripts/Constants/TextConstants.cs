﻿public static class TextConstants
{
    public const string StartServerFormat = "StartGameServer in ip: {0}";
    public const string StartBroadCast = "StartBroadcast";
    public const string GameServer = "GameServer";

    public const string RunAsClient = "Run as Client";
    public const string RunAsServer = "Run as server";
    public const string NetworkStatusFormat = "Network status: {0}";
    public const string SearchServer = "Search server";
    public const string WaitingSecondPlayer = "Waiting second player";

    public const string MineVictoryLabel = "Вы выиграли";
    public const string EnemyVictoryLabel = "Соперник победил";
    public const string MineTimeFormat = "Ваше время  {0}";
    public const string MineScoreFormat = "Ваши очки {0}";
    public const string EnemyTimeFormat = "Время соперника {0}";
    public const string EnemyScoreFormat = "Очки соперника {0}";
}
