﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ShipsManager : MonoBehaviour
{
    public Action<string> ShipFinishedEvent;

    [SerializeField]
    private Track m_track;
    [SerializeField]
    private CameraFollowShip m_cameraFollow;
    [SerializeField]
    private GameObject m_firstSpawnPoint;
    [SerializeField]
    private GameObject m_secondSpawnPoint;
    [SerializeField]
    private GameObject m_firstShipPrefab;
    [SerializeField]
    private GameObject m_secondShipPrefab;
    [SerializeField]
    private SyncedTime m_shipsSharedTime;

    private List<Ship> m_ships;
    private SpaceSceneModel m_spaceSceneModel;

    public void StartMoveShips()
    {
        m_shipsSharedTime.SetStartTime(m_spaceSceneModel.StartRaceTime);
        for (int i = 0; i < m_ships.Count; i++)
        {
            m_ships[i].StartMove();
        }
    }

    public void SpawnShips(SpaceSceneModel spaceSceneModel)
    {
        m_ships = new List<Ship>();
        if(Network.isServer)
        {
            var ship = SpawnShip(m_firstShipPrefab, m_firstSpawnPoint.transform.position, spaceSceneModel);
            m_cameraFollow.SetTarget(ship.GetShipRoot().transform);
            return;
        }

        if(Network.isClient)
        {
            var ship = SpawnShip(m_secondShipPrefab, m_secondSpawnPoint.transform.position, spaceSceneModel);
            m_cameraFollow.SetTarget(ship.GetShipRoot().transform);
        }
    }

    private void Update()
    {
        //ссылку на m_shipsSharedTime получают оба корабля, так у них получается одинаковое deltaTime
        //и по идее они не должны дёргаться при движении, но они всё равно дёргаются, надо другое решение
        m_shipsSharedTime.Elapse();
    }

    private Ship SpawnShip(GameObject prefab, Vector3 position, SpaceSceneModel spaceSceneModel)
    {
        m_spaceSceneModel = spaceSceneModel;
        var shipGameObject = Network.Instantiate(prefab, position, Quaternion.identity, 0) as GameObject;
        var ship = shipGameObject.GetComponent<Ship>();
        return ship;
    }

    public void ShipFinish(string shipModelId)
    {
        if(ShipFinishedEvent != null)
        {
            ShipFinishedEvent(shipModelId);
        }
    }

    public void InitShip(Ship ship)
    {
        var modelId = m_spaceSceneModel.AddPlayerModel(ship.networkView.isMine);
        ship.Init(modelId, m_spaceSceneModel, m_track, m_shipsSharedTime);
        m_ships.Add(ship);
    }
}
