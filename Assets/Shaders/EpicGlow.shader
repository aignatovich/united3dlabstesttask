Shader "R/EpicGlow"
{
	Properties 
	{
_RimColor("_RimColor", Color) = (1,1,1,1)
_RimFaloff("_RimFaloff", Range(0.1,3) ) = 3
_RimNormal("_RimNormal", Range(0,1) ) = 0.341
_RimVertex("_RimVertex", Range(0,1) ) = 0.337
_Diffuse("_Diffuse", Range(0.1,1) ) = 1
_SpecFaloff("_SpecFaloff", Range(0,1) ) = 1
_Glossiness("_Glossiness", Range(0,1) ) = 1
_SpecularColor("_SpecularColor", Color) = (0.5970149,0.9661831,1,1)
_MainTex("Base (RGB) Gloss (A)", 2D) = "white" {}
_BumpMap("Normalmap", 2D) = "bump" {}
_Emit("_Emit", 2D) = "black" {}
_Emission("_Emission", Range(0,1) ) = 0.5

	}
	
	SubShader 
	{
		Tags
		{
"Queue"="Geometry"
"IgnoreProjector"="False"
"RenderType"="Opaque"

		}

		
Cull Back
ZWrite On
ZTest LEqual
ColorMask RGBA
Fog{
}


		CGPROGRAM
#pragma surface surf BlinnPhongEditor  vertex:vert
#pragma target 3.0


float4 _RimColor;
float _RimFaloff;
float _RimNormal;
float _RimVertex;
float _Diffuse;
float _SpecFaloff;
float _Glossiness;
float4 _SpecularColor;
sampler2D _MainTex;
sampler2D _BumpMap;
sampler2D _Emit;
float _Emission;

			struct EditorSurfaceOutput {
				half3 Albedo;
				half3 Normal;
				half3 Emission;
				half3 Gloss;
				half Specular;
				half Alpha;
				half4 Custom;
			};
			
			inline half4 LightingBlinnPhongEditor_PrePass (EditorSurfaceOutput s, half4 light)
			{
half3 spec = light.a * s.Gloss;
half4 c;
c.rgb = (s.Albedo * light.rgb + light.rgb * spec);
c.a = s.Alpha;
return c;

			}

			inline half4 LightingBlinnPhongEditor (EditorSurfaceOutput s, half3 lightDir, half3 viewDir, half atten)
			{
				half3 h = normalize (lightDir + viewDir);
				
				half diff = max (0, dot ( lightDir, s.Normal ));
				
				float nh = max (0, dot (s.Normal, h));
				float spec = pow (nh, s.Specular*128.0);
				
				half4 res;
				res.rgb = _LightColor0.rgb * diff;
				res.w = spec * Luminance (_LightColor0.rgb);
				res *= atten * 2.0;

				return LightingBlinnPhongEditor_PrePass( s, res );
			}

			inline half4 LightingBlinnPhongEditor_DirLightmap (EditorSurfaceOutput s, fixed4 color, fixed4 scale, half3 viewDir, bool surfFuncWritesNormal, out half3 specColor)
			{
				UNITY_DIRBASIS
				half3 scalePerBasisVector;
				
				half3 lm = DirLightmapDiffuse (unity_DirBasis, color, scale, s.Normal, surfFuncWritesNormal, scalePerBasisVector);
				
				half3 lightDir = normalize (scalePerBasisVector.x * unity_DirBasis[0] + scalePerBasisVector.y * unity_DirBasis[1] + scalePerBasisVector.z * unity_DirBasis[2]);
				half3 h = normalize (lightDir + viewDir);
			
				float nh = max (0, dot (s.Normal, h));
				float spec = pow (nh, s.Specular * 128.0);
				
				// specColor used outside in the forward path, compiled out in prepass
				specColor = lm * _SpecColor.rgb * s.Gloss * spec;
				
				// spec from the alpha component is used to calculate specular
				// in the Lighting*_Prepass function, it's not used in forward
				return half4(lm, spec);
			}
			
			struct Input {
				float2 uv_MainTex;
float2 uv_BumpMap;
float3 viewDir;
float2 uv_Emit;

			};

			void vert (inout appdata_full v, out Input o) {
float4 VertexOutputMaster0_0_NoInput = float4(0,0,0,0);
float4 VertexOutputMaster0_1_NoInput = float4(0,0,0,0);
float4 VertexOutputMaster0_2_NoInput = float4(0,0,0,0);
float4 VertexOutputMaster0_3_NoInput = float4(0,0,0,0);


			}
			

			void surf (Input IN, inout EditorSurfaceOutput o) {
				o.Normal = float3(0.0,0.0,1.0);
				o.Alpha = 1.0;
				o.Albedo = 0.0;
				o.Emission = 0.0;
				o.Gloss = 0.0;
				o.Specular = 0.0;
				o.Custom = 0.0;
				
float4 Tex2D1=tex2D(_MainTex,(IN.uv_MainTex.xyxy).xy);
float4 Multiply2=Tex2D1 * _Diffuse.xxxx;
float4 Tex2D0=tex2D(_BumpMap,(IN.uv_BumpMap.xyxy).xy);
float4 UnpackNormal0=float4(UnpackNormal(Tex2D0).xyz, 1.0);
float4 Fresnel1_1_NoInput = float4(0,0,1,1);
float4 Fresnel1=(1.0 - dot( normalize( float4( IN.viewDir.x, IN.viewDir.y,IN.viewDir.z,1.0 ).xyz), normalize( Fresnel1_1_NoInput.xyz ) )).xxxx;
float4 Multiply9=Fresnel1 * _RimVertex.xxxx;
float4 Fresnel0=(1.0 - dot( normalize( float4( IN.viewDir.x, IN.viewDir.y,IN.viewDir.z,1.0 ).xyz), normalize( UnpackNormal0.xyz ) )).xxxx;
float4 Multiply8=Fresnel0 * _RimNormal.xxxx;
float4 Add1=Multiply9 + Multiply8;
float4 Pow0=pow(Add1,_RimFaloff.xxxx);
float4 Multiply0=_RimColor * Pow0;
float4 Tex2D2=tex2D(_Emit,(IN.uv_Emit.xyxy).xy);
float4 Multiply10=Tex2D2 * _Emission.xxxx;
float4 Add2=Multiply0 + Multiply10;
float4 Luminance0= Luminance( Multiply10.xyz ).xxxx;
float4 Master0_7_NoInput = float4(0,0,0,0);
float4 Master0_6_NoInput = float4(1,1,1,1);
o.Albedo = Multiply2;
o.Normal = UnpackNormal0;
o.Emission = Add2;
o.Specular = _Glossiness.xxxx;
o.Gloss = _SpecularColor;
o.Alpha = Luminance0;

				o.Normal = normalize(o.Normal);
			}
		ENDCG
	}
	Fallback "Diffuse"
}